# TypeScript course

# Recursos
- Documentación - https://www.typescriptlang.org/
- Compilador en línea - https://www.typescriptlang.org/play/
- TS en React - https://reactjs.org/docs/static-type-checking.html#typescript 
- TS en Vue - https://vuejs.org/v2/guide/typescript.html

# How to install
- $npm i -g typescript

# Run TS compiler
- $tsc app.ts
